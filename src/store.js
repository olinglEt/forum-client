import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentThread: {
      title: ""
    },
    token: "",
    threadList: [],
    messageList: []
  },
  mutations: {
    setThreadList(state, payload) {
      state.threadList = payload.threads;
    },
    setMessageList(state, payload) {
      state.messageList = payload.messages;
    },
    setCurrentThread(state, payload) {
      state.currentThread.title = payload.title;
    },
    setToken(state, payload) {
      state.token = payload.token;
    }
  },
  actions: {
    fetchThreadList({ commit }) {
      axios.get(process.env.VUE_APP_ENDPOINT_API).then(response => {
        commit("setThreadList", response.data);
      });
    },
    fetchThread({ commit }, payload) {
      axios
        .get(process.env.VUE_APP_ENDPOINT_API + "/thread/show/" + payload.id)
        .then(response => {
          commit("setMessageList", response.data);
        });
    },
    createThread({ commit, getters }, payload) {
      return new Promise((resolve, reject) => {
        let params = new URLSearchParams();
        params.append("title", payload.title);
        params.append("token", getters["token"]);
        axios
          .post(process.env.VUE_APP_ENDPOINT_API + "/thread/add", params)
          .then(response => {
            commit("setThreadList", response.data);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    postMessage({ commit, getters }, payload) {
      return new Promise((resolve, reject) => {
        let params = new URLSearchParams();
        params.append("id", payload.id);
        params.append("content", payload.content);
        params.append("token", getters["token"]);
        axios
          .post(process.env.VUE_APP_ENDPOINT_API + "/message/add", params)
          .then(response => {
            commit("setMessageList", response.data);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    signUp(context, payload) {
      return new Promise((resolve, reject) => {
        let params = new URLSearchParams();
        params.append("username", payload.username);
        params.append("display_name", payload.displayName);
        params.append("password", payload.password);
        axios
          .post(process.env.VUE_APP_ENDPOINT_AUTH + "/register", params)
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    },
    signIn({ commit }, payload) {
      return new Promise((resolve, reject) => {
        let params = new URLSearchParams();
        params.append("username", payload.username);
        params.append("password", payload.password);
        axios
          .post(process.env.VUE_APP_ENDPOINT_AUTH + "/signin", params)
          .then(response => {
            commit("setToken", response.data);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    validateToken(context) {
      let params = new URLSearchParams();
      params.append("token", context.getters["token"]);
      axios
        .post(process.env.VUE_APP_ENDPOINT_AUTH + "/validate", params)
        .then(() => {
          alert("valid token");
        })
        .catch(err => {
          alert(err);
        });
    },
    remove({ getters }) {
      let params = new URLSearchParams();
      params.append("token", getters["token"]);
      return new Promise((resolve, reject) => {
        axios
          .post(process.env.VUE_APP_ENDPOINT_API + "/remove", params)
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          });
      });
    }
  },
  getters: {
    threadList({ threadList }) {
      return threadList;
    },
    messageList({ messageList }) {
      return messageList;
    },
    currentThread({ currentThread }) {
      return currentThread;
    },
    token({ token }) {
      return token;
    }
  }
});
