import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Thread from "./views/Thread.vue";
import CreateThread from "./views/CreateThread";
import SignUp from "./views/SignUp";
import SignIn from "./views/SignIn";
import MyPage from "./views/MyPage";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/thread/show/:id",
      name: "thread",
      component: Thread
    },
    {
      path: "/thread/add",
      name: "createThread",
      component: CreateThread
    },
    {
      path: "/signin",
      name: "signin",
      component: SignIn
    },
    {
      path: "/signup",
      name: "signup",
      component: SignUp
    },
    {
      path: "/mypage",
      name: "mypage",
      component: MyPage
    }
  ]
});
